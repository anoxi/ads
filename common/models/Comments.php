<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "comments".
 *
 * @property int $id
 * @property string $content
 * @property int $author_id
 * @property int $advertise_id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Advertise $advertise
 * @property User $author
 */
class Comments extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 10;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'advertise_id', 'status'], 'required'],
            [['author_id', 'advertise_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string', 'max' => 255],
            [['advertise_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advertise::className(), 'targetAttribute' => ['advertise_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'author_id' => 'Author ID',
            'advertise_id' => 'Advertise ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertise()
    {
        return $this->hasOne(Advertise::className(), ['id' => 'advertise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return $this
     */
    public function findActive(){
        return self::find()->where(['status' => self::STATUS_ACTIVE]);
    }

    public function saveComment($userId){
        $this->author_id = $userId;
        return self::save();
    }
}
