<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'content' => $this->string(),
            'author_id' => $this->integer()->notNull(),
            'advertise_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);


        $this->createTable('{{%advertise}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('comment_to_user', 'comments', 'author_id', 'user', 'id');
        $this->addForeignKey('comment_to_advertise', 'comments', 'advertise_id', 'advertise', 'id');

        $this->addForeignKey('advertise_to_user', 'advertise', 'author_id', 'user', 'id');

    }

    public function down()
    {
        $this->dropForeignKey('comment_to_user', 'comments');
        $this->dropForeignKey('comment_to_advertise', 'comments');
        $this->dropForeignKey('advertise_to_user', 'advertise');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%comments}}');
        $this->dropTable('{{%advertise}}');
    }
}
