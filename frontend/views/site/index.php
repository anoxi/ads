<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//var_dump($dataProvider->getCount()); die();
$this->title = 'Ads';
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_list',
        'options' => [
            'tag' => 'div',
            'class' => 'ads-list',
            'id' => 'ads-list',
        ],
        'emptyText' => 'List is empty',
    ]); ?>
</div>
