<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\Comments;
?>

<h2>
    <?= Html::encode($model->title) ?>
</h2>
<h3>
    <?= Html::encode($model->content) ?>
</h3>
<?php
    if(isset($model->updated_at)){
        echo '<div class="cUpdated">' . date("Y-m-d H:i:s", $model->updated_at) . '</div>';
    }else{
        echo '<div class="cCreated">' . date("Y-m-d H:i:s", $model->created_at) . '</div>';
    }
?>
<hr>
<?php
    if(count($model->comments) > 0){
        echo '<div class="cmments" style="padding: 1em 0;">';
            foreach ($model->comments as $comment) {
                echo '<div class="singleCommentsCnt" style="padding: 1em 0; border-bottom:1px solid gainsboro;padding: 0.3em;">';
                echo '<div class="cAuthor" style="color:#adadad;">' . $comment->author->username . '</div>';
                echo '<div class="cContent" style="font-size: 1.4em;">' . Html::encode($comment->content) . '</div>';
                if(isset($comment->updated_at)){
                    echo '<div class="cUpdated" style="font-style: italic; font-size:0.8em;color:#adadad;">' . date("Y-m-d H:i:s", $comment->updated_at) . '</div>';
                }else{
                    echo '<div class="cCreated" style="font-style: italic; font-size:0.8em;color:#adadad;">' . date("Y-m-d H:i:s", $comment->created_at) . '</div>';
                }
                echo '</div>';
            }
        echo '</div>';
    }
?>
<div class="comments-form">
    <?php
    if(!Yii::$app->user->isGuest) {
        $comments = new Comments();
        $form = ActiveForm::begin(['action' => ['/site/save-comment']]);
        ?>

        <?= $form->field($comments, 'content')
            ->textarea(['maxlength' => true, 'style' => 'max-width: 500px; height: 4em;'])
            ->label('Text Comment')
        ?>

        <?= Html::hiddenInput('Comments[advertise_id]', $model->id); ?>
        <?= Html::hiddenInput('Comments[status]', Comments::STATUS_ACTIVE); ?>

        <div class="form-group">
            <?= Html::submitButton('Add Comment', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end();
    }?>

</div>
<?php
echo Html::a('Back to ads', ['/site/'], ['class'=>'btn btn-default']);
