<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
/** @var \common\models\Advertise $model */
?>

<div class="news-item">
    <h2><?= Html::encode($model->title) ?></h2>
    <?= HtmlPurifier::process($model->content) ?>
    <br>
    Author: <?= $model->author->username ?>
    <br>
    <?= count($model->comments) > 0 ? 'comments: ' . count($model->comments) : '' ?>
    <br>
    <?= Html::a('View Ads', ['/site/view', 'id' => $model->id], ['class'=>'btn btn-default']) ?>
</div>