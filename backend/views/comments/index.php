<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\Comments;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Comments', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'content',
            'author_id',
            'advertise_id',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'filter' => [
                    Comments::STATUS_ACTIVE => 'Active',
                    Comments::STATUS_DELETED=> 'Deleted'
                ]
            ],

            ['header'=>'Actions','class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
