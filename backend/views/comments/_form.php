<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\Comments;

/* @var $this yii\web\View */
/* @var $model common\models\Comments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'content')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author_id')->textInput() ?>

    <?= $form->field($model, 'advertise_id')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([
        Comments::STATUS_ACTIVE => 'Active',
        Comments::STATUS_DELETED => 'Deleted'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
