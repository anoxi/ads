<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Advertise;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AdvertiseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Advertises';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertise-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Advertise', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'content:ntext',
            [
                'attribute' => 'author_id',
                'label' => 'Author id',
                'contentOptions'=>['style'=>'min-width: 100px;']
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'filter' => [
                    Advertise::STATUS_ACTIVE => 'Active',
                    Advertise::STATUS_DELETED=> 'Deleted'
                ]
            ],
            ['header'=>'Actions','class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
